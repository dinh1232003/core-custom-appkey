# from typing import Union
from pydantic import BaseModel
from fastapi import FastAPI
import requests
app = FastAPI()


class LoginInfo(BaseModel):
    userName: str | None = ""
    passWord: str | None = ""
    user_agent: str 
    app_key: str


class UserInfo(BaseModel):
    session_id: str
    token: str
    user_agent: str 
    # app_key: str


class getLinkFshare(BaseModel):
    session_id: str
    token: str
    link: str
    user_agent: str 
    # app_key: str
    password: str | None = ""


"""
* Param: Email and Password of Fshare account
* Return: session_id and token of account

"""


def loginFshareAccount(userLogin):
    if (userLogin.userName == ""):
        return {"msg": "Username not empty"}
    if (userLogin.passWord == ""):
        return {"msg": "Password not empty"}
    # return userName, passWord
    url = "https://api.fshare.vn/api/user/login"
    headers = {
        "Host": "api.fshare.vn",
        "Content-Type": "application/json; charset=UTF-8",
        "Connection": "keep-alive",
        "Accept": "*/*",
        "User-Agent": userLogin.user_agent,
        "Accept-Language": "en-VN;q=1.0",
        "Accept-Encoding": "br;q=1.0, gzip;q=0.9, deflate;q=0.8"
    }
    userInfo = {
        "user_email": userLogin.userName,
        "app_key": userLogin.app_key,
        "password": userLogin.passWord
    }
    return requests.post(url=url, headers=headers, json=userInfo).json()


# def getInfoFshareAccount():
#     pass


# @app.get("/")
# def read_root():
#     return {"Hello": "World"}

"""
* Param: userName and passWord
* Return: session_id and token of account
"""
@app.post("/login")
def loginHandle(userLogin: LoginInfo):
    return loginFshareAccount(userLogin)

"""
* Param: session_id and token
* Return: Infomation of Fshare account
"""
@app.post("/info")
def getInfoFshareAccount(loginAccount: UserInfo):
    url = "https://api.fshare.vn/api/user/get"
    header = {
        "Host": "api.fshare.vn",
        "Accept": "*/*",
        "Cookie": "session_id=" + loginAccount.session_id,
        "User-Agent": loginAccount.user_agent,
        "Accept-Language": "en-VN;q=1.0",
        "Accept-Encoding": "br;q=1.0, gzip;q=0.9, deflate;q=0.8",
        # "Content-Type": "application/json; charset=UTF-8",
        "Connection": "keep-alive",
    }
    return requests.get(url=url, headers=header).json()


@app.post("/getlink")
def getlinks(getlink: getLinkFshare):
    url = "https://api.fshare.vn/api/session/download"
    # getlink.link = getlink.link.replace("/", "\/")
    header = {
        "Host": "api.fshare.vn",
        "Connection": "keep-alive",
        "Accept": "*/*",
        "Fshare-Session-id": getlink.session_id,
        "Accept-Language": "en-us",
        "User-Agent": getlink.user_agent,
        "Content-Type": "application/json",
        "Accept-Encoding": "gzip, deflate, br"
    }
    data = {
        "token": getlink.token,
        "zipflag": "0",
        "password": getlink.password,
        "url": getlink.link
       
    }
    # return header, data
    return requests.post(url=url, headers=header, json=data).json()
