FROM amd64/alpine:3.17
COPY main.py /main.py
ENV PYTHONUNBUFFERED=1
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools
RUN pip3 install uvicorn
RUN pip3 install fastapi
RUN python3 -m pip install requests
CMD ["uvicorn", "main:app", "--port", "8080", "--host", "0.0.0.0"]
EXPOSE 8080:8080
